import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import Wompi from './screens/Wompi/index';


const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Wompi/>} />
      </Routes>
    </Router>
  );
}

export default App;
