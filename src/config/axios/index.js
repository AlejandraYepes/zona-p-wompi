import axios from 'axios/index';

const instance = axios.create({
    baseURL: 'https://zonap.test.leancore.co/',
    timeout: 1000 * 20,


})

export default instance;