import userApp from '../../assets/images/user-app.jpeg';
import wompi from '../../assets/images/wompi-logo.png';
import wompiView from '../../assets/images/wompi-preview.png';
import bill from '../../assets/images/bill.png';
import pointer from '../../assets/images/pointer.png';
import select from '../../assets/images/select.png';


const cards = [
    {
        index: 1,
        type: 'user-qpp',
        title: 'Seleccione su mensualidad',
        description: 'En la App de Zona P seleccione el vehículo y el monto de su mensualidad. Verifique que la sede corresponda a la que usted frecuenta.',
        topLogo: bill
    },
    {
        index: 2,
        type: 'wompi',
        title: 'Haga click en el enlace de pago',
        description: 'Generamos aquí un enlace seguro para tu pago a través de nuestra plataforma',
        image: wompi,
        topLogo: pointer
    },
    {
        index: 3,
        type: 'pay',
        title: 'Elige la opción de pago',
        image: wompiView,
        topLogo: select
    }];

export default cards;