import { createTheme } from '@mui/material';


const theme = createTheme({
    palette: {
        primary: {
            main: '#00A9A0'
        },
        secondary: {
            main: '#FFF200'
        }

    },
    typography: {
        fontFamily: [
            'Titillium Web'
        ],
        body1: {
            fontWeight: 600 
        }

    }

});

export default theme;