import React, { useState, useEffect } from 'react';
import { useLocation } from "react-router-dom";
import queryString from "query-string";
// MUI 
import {
    Typography,
    AppBar,
    Card,
    CardContent,
    CssBaseline,
    Grid,
    Toolbar,
    Button,
    Paper
} from '@mui/material';
import { ThemeProvider } from '@mui/material';
import theme from '../../themeConfig';
import useStyles from './styles';
// IMAGES
import zonap from '../../assets/images/zonap-logo.png';
import leancore from '../../assets/images/leancore-logo.png';
import googlePlay from '../../assets/images/google-play.png';
import appStore from '../../assets/images/app-store.png';
import celZonap from '../../assets/images/celZonap.png';
// Components
import InfoForm from '../../components/form';
// Constants
import cards from '../../config/constants/cardsInfo';
import instance from '../../config/axios';
import { WOMPI_REFERENCE_DATA } from '../../config/api';

const Wompi = () => {
    const classes = useStyles();
    const location = useLocation();
    const [loading, setLoading] = useState(true);
    const [userData, setUserData] = useState({
        // status: "due",
        // monthlyUser: true,
        // hqId: "iIJJcbIpMdVeYwEDK6mJ",
        // total: 70000,
        // validity: {
        //     _seconds: 1638766799,
        //     _nanoseconds: 59000000
        // },
        // userPhone: "+570000000000",
        // userName: "Don Juan",
        // officialEmail: "funcionario@zonap.test",
        // vehicleType: "car",
        // userId: "DBVGLz45flgKRIliqLc6",
        // plates: [
        //     "HBH888"
        // ],
        // type: "personal",
        // capacity: 1,
        // parkedPlatesList: [],
        // id: "HXk5SzpSQDC84HXWLW3E"
    });
    const [showWompiForm, setShowWompiForm] = useState(true);
    const [showSuccessModal, setShowSuccessModal] = useState();
    const [showErrorModal, setErrorModal] = useState();

    useEffect(() => {
        if (location.search !== "") {
            console.info(location.search);
            const query = queryString.parse(location.search);
            console.info(query.reference);
            (async () => {
                if (query.reference) {
                    try {
                        const userRef = query.reference;
                        const { data } = await instance.post(WOMPI_REFERENCE_DATA, { reference: userRef });
                        console.log(data)
                        const user = data?.data?.mensualityData;
                        setUserData(user);
                        setLoading(false);
                    } catch (err) {
                        console.log('USERDATA', userData)
                        setLoading(false);
                        console.info(err.response);
                    }
                }
            })();
        } else {
            setLoading(false);
        }
    }, [location])

    return (
        <ThemeProvider theme={theme}>
            <div className={classes.background}>
                <div className={classes.backgroundTint}>
                    <CssBaseline />
                    <AppBar
                        position="sticky"
                        className={classes.header}
                        style={{ backgroundColor: 'rgba(0,0,0,0.6)' }}
                    >
                        <Toolbar className={classes.toolbar}>
                            <Grid className={classes.logo}>
                                <img src={zonap} alt="logo" style={{ width: 100 }} />
                            </Grid>
                            {/* <Grid className={classes.gridNav}>
              <nav className={classes.nav}>
                <ul className={classes.navList}>
                  <li>DONDE ESTAMOS</li>
                  <li>PARQUEA MÁS FÁCIL</li>
                </ul>
              </nav>
            </Grid> */}

                        </Toolbar>
                    </AppBar>
                    <main>
                        <div className={classes.container}>
                            <Grid container style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'center',
                            }} >
                                <Grid item xs={12} sm={12} md={5}>
                                    <Paper className={classes.paper} style={{ borderRadius: '30px' }}>
                                        <InfoForm info={userData} />
                                    </Paper>
                                </Grid>
                                <Grid item className={classes.containerInfo} xs={12} sm={7} md={7}>
                                    <Grid container className={classes.downloadInfo}>
                                        <Grid item xs={12} sm={6} md={4} >
                                            <Typography variant="h5" align="center" color="#8D8787" paragraph >
                                                Con la app de zona p ingresa y paga tu parqueo desde tu celular
                                            </Typography>
                                            <Typography variant="h5" align="center" color="#00A9A0" paragraph>
                                                Descarga en
                                            </Typography>
                                            <Grid container spacing={2} justifyContent="center" >
                                                <Button
                                                    href={"https://play.google.com/store/apps/details?id=com.parkingpayments"}
                                                >
                                                    <img src={googlePlay} alt="" />
                                                </Button>
                                                <Button
                                                    href={"https://apps.apple.com/co/app/zona-p/id1576261346"}
                                                >
                                                    <img src={appStore} alt="" />
                                                </Button>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} sm={6} md={6}>
                                            <img src={celZonap} alt="cellphones" style={{ width: '100%' }} />
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} sm={10} md={10}>
                                        <Typography variant="h5" align="center" color="#000F3D" paragraph>
                                            Así de fácil es pagar tu mensualidad en zona p
                                        </Typography>
                                    </Grid>
                                    <Grid container spacing={4} xs={12} sm={10} md={10} style={{ margin: '0px 10px 0px 4px', padding: '5px'}} align='center'>
                                        {cards.map((card) => (
                                            <Grid item key={card.index} xs={12} sm={6} md={4} align='center' style={{ display: 'flex', justifyContent: 'space-between', padding: '10px' }}>
                                                <Card className={classes.card} style={{ borderRadius: '6%' }} align='center' xs={12} >
                                                    <CardContent className={classes.CardContent} >
                                                        <Grid container>
                                                            <Grid item xs={12} sm={6} md={6} style={{ display: 'flex', flexDirection: 'column', alignItems: 'space-between' }}>
                                                                <Typography gutterBottom fontSize={16} variant="h8" color="#000F3D" align="center" xs={12} sm={12} md={12}>
                                                                    {card.index}
                                                                </Typography>
                                                                <Typography gutterBottom fontSize={13} variant="h8" color="#000F3D" align="center" xs={12} sm={12} md={12}>
                                                                    {card.title}
                                                                </Typography>
                                                            </Grid>
                                                            <Grid item xs={12} sm={6} md={6} style={{ alignSelf: 'center' }} align='center'>
                                                                <img src={card.topLogo} alt="toplogo" style={{ width: 70 }} />
                                                            </Grid>

                                                        </Grid>
                                                        {card.description &&
                                                            <Grid container >
                                                                <Grid item paddingTop={6}>
                                                                    <Typography fontSize={12} color="#000F3D" align="center" paragraph >
                                                                        {card.description}
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        }

                                                    </CardContent>
                                                    <Grid xs={12}>
                                                        {card.image && <img src={card.image} alt="logo" style={{ width: '100%', paddingBottom: 15 }} />}

                                                    </Grid>

                                                </Card>
                                            </Grid>
                                        ))}
                                    </Grid>
                                    <footer className={classes.footer}>
                                        <Typography variant="subtitle1" align="center" color="#000F3D">UNA ALIANZA CON</Typography>
                                        <img src={leancore} alt="lean logo" style={{ width: 179 }} />
                                    </footer>
                                </Grid>
                            </Grid>
                        </div>
                    </main>

                </div>
                {/* <CustomModal
                    openModal={showWompiForm}
                    setOpenModal={setShowWompiForm}
                /> */}

            </div>

        </ThemeProvider>
    )
};

export default Wompi;