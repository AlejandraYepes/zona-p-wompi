import { makeStyles } from '@mui/styles';
import park from '../../assets/images/park.png';


const useStyles = makeStyles((theme) => ({
    background: {
        backgroundImage: `url(${park})`,
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '100%',
        backgroundSize: "cover"
    },
    backgroundTint:{
        backgroundColor: 'rgba(0, 169, 160, 0.7)'
    },
    header: {
        padding: '15px 0px',
    },
    toolbar:{
        justifyContent: 'space-between'
    },
    gridNav: {
        width: '40%'
    },
    nav: {
        width: '100%'
    },
    navList: {
        listStyle: 'none',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: 'white'
    },
    container: {
        padding: '0 0px',
        display: 'flex',

    },
    containerInfo: {
        backgroundColor: 'rgba(255, 255, 255, 0.88)',
        padding: '40px 30px 30px 30px',
        textTransform: 'upperCase',
    },
    downloadInfo:{
        display: 'flex',
        flexDirection: 'row'
    },
    logo: {
        marginLeft: '11%',
        flexGrow: 1
    },
   
    cardGrid: {
        padding: '20px 0'
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        textTransform: 'none',
        padding: '10px 15px'
        
    },
    cardContent: {
        flexGrow: 1,
    },
    
    paper:{
        margin: '30px 40px',
        // padding: '24px 20px',
        borderRadius: '6%',
        width: '80%'

    },
    footer: {
        padding: '20px 0',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#FFFFFF',
    }

}));

export default useStyles;