import React from 'react';
import { Dialog, DialogTitle, DialogContent } from '@mui/material';

const CustomModal = (props) => {

    const { title, children, openModal, setOpenModal } = props;

    return (
        <Dialog open={openModal}>
            <DialogTitle>
                <div>title </div>

            </DialogTitle>
            <DialogContent>
                <div>content </div>
             

            </DialogContent>
        </Dialog>
    )
};

export default CustomModal;