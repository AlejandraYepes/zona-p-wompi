import { makeStyles } from '@mui/styles';


const useStyles = makeStyles((theme) => ({
    root: {
        padding: '0px',
    },
    payButton: {
        color: '#00A9A0',
        padding: '15px 25px',
        borderRadius: '10px',
        borderWidth: 0,
        letterSpacing: 5,
        backgroundColor: '#FFF200',
        fontWeight: 600,
        fontSize: 15,
        margin: '30px 0px 20px 0px'
    },
    plateInput:{
        display: 'flex',
        backgroundColor: '#00A9A0',
        border: 'none',
        borderRadius: '10px',
        margin:'5px',
        padding: '15px 10px',
        width: '80px',
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: 600 
    },
    phoneInput:{
        backgroundColor: '#E8E8E8',
        border: 'none',
        borderRadius: '20px',
        padding: '15px 20px',
        margin: '10px',
        width: '95%',
        color: "#00A9A0",
        fontWeight: 600,
        fontSize: 15,
    }, 
    formHeader:{
        backgroundColor: '#00A9A0',
        borderRadius: '30px 30px 0px 0px',
        paddingTop: '70px'

    },
    formTitle:{
        backgroundColor: '#FFFFFF',
        width: '80%',
        padding: '40px 0px 30px 0px',
        borderRadius: '30px 30px 0px 0px',

    }


}));

export default useStyles;