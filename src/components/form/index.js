import React from 'react';
import { Grid, Typography } from '@mui/material';
import useStyles from './styles';
import moment from 'moment-timezone';
import carLogo from '../../assets/images/car-outerline.png'
import numberWithPoints from '../../config/services/numberWithPoints';

const InfoForm = (props) => {
    const { title, children, showForm, hideForm, info } = props;
    const classes = useStyles();
    const validity = info.validity !== undefined ? info.validity : "";
    const month = validity !== "" ? moment(validity._seconds * 1000).format('YYYY-MM-DD HH:mm:ss') : '';

    return (
        <>
            <form className={classes.root}>
                <Grid item xs={12} sm={12} md={12} align="center" style={{ marginBottom: '10px' }}>
                    <div className={classes.formHeader}>
                        <div className={classes.formTitle}>
                            <Typography color="primary">PAGUE SU MENSUALIDAD AHORA</Typography>
                        </div>
                    </div>
                    <img src={carLogo} alt="carLogo" />
                    <Typography color='#868686'>Mis vehículos / Placas asociadas</Typography>

                </Grid>
                <Grid container xs={12} sm={12} md={12} align="center" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', padding: '0px 25px' }}>
                    <Grid item xs={6} sm={6} md={2} >
                        <input
                            type="text"
                            // label="Placa"
                            value={info.plates !== undefined ? info.plates[0] : ''}
                            // style={{ bakcgroundColor: '#00A9A0' }}
                            className={classes.plateInput}
                            readOnly
                        // color="secondary"
                        />
                    </Grid>
                    <Grid item xs={6} sm={6} md={2} >
                        <input
                            type="text"
                            // label="Placa"
                            value={info.plates !== undefined ? info.plates[1] : ''}
                            // style={{ bakcgroundColor: '#00A9A0' }}
                            className={classes.plateInput}
                            // color="secondary"
                            readOnly

                        />
                    </Grid>
                    <Grid item xs={6} sm={6} md={2}>
                        <input
                            type="text"
                            // label="Placa"
                            value={info.plates !== undefined ? info.plates[2] : ''}
                            // style={{ bakcgroundColor: '#00A9A0' }}
                            className={classes.plateInput}
                            // color="secondary"
                            readOnly

                        />
                    </Grid>
                    <Grid item xs={6} sm={6} md={2}>
                        <input
                            type="text"
                            // label="Placa"
                            value={info.plates !== undefined ? info.plates[3] : ''}
                            // style={{ bakcgroundColor: '#00A9A0' }}
                            className={classes.plateInput}
                            // color="secondary"
                            readOnly

                        />
                    </Grid>
                    <Grid item xs={6} sm={6} md={2}>
                        <input
                            type="text"
                            // label="Placa"
                            value={info.plates !== undefined ? info.plates[4] : ''}
                            // style={{ bakcgroundColor: '#00A9A0' }}
                            className={classes.plateInput}
                            // color="secondary"
                            readOnly

                        />
                    </Grid>
                </Grid>
                <Grid item xs={12} style={{ padding: '0px 30px' }}>
                    <label htmlFor="phone" style={{ marginLeft: '30px', fontSize: 13 }}>Celular</label>
                    <input
                        id="phone"
                        type="text"
                        value={info.userPhone !== undefined ? info.userPhone : ''}
                        className={classes.phoneInput}
                        readOnly
                    />
                </Grid>
                <Grid item xs={12} style={{ padding: '0px 30px' }}>
                    <label htmlFor="userName" style={{ marginLeft: '30px', fontSize: 13 }}>Persona responsable</label>
                    <input
                        id="userName"
                        type="text"
                        value={info.userName !== undefined ? info.userName : ''}
                        className={classes.phoneInput}
                        readOnly
                    />
                </Grid>
                <Grid item xs={12} style={{ padding: '0px 30px' }}>
                    <label htmlFor="validity" style={{ marginLeft: '30px', fontSize: 13 }}>Válido hasta</label>
                    <input
                        id="validity"
                        type="text"
                        value={month !== undefined ? month : ''}
                        className={classes.phoneInput}
                        readOnly
                    />
                </Grid>
                <Grid item xs={12} style={{ padding: '0px 30px' }}>
                    <label htmlFor="total" style={{ marginLeft: '30px', fontSize: 13 }}>Valor a pagar</label>
                    <input
                        id="total"
                        type="text"
                        value={info.total !== undefined ? '$' + numberWithPoints(info.total) : ''}
                        className={classes.phoneInput}
                        readOnly
                    />
                </Grid>
                <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                    <form action="https://checkout.wompi.co/p/" method="GET">
                        <input type="hidden" name="public-key" value="pub_test_sYUdKyDD9NWG1MR5zhVR4JGdIN2bpW0S" />
                        <input type="hidden" name="currency" value="COP" />
                        <input type="hidden" name="amount-in-cents" value="7650000" />
                        <input type="hidden" name="reference" value="dYKrwjtTTtmQWehZg2vj" />
                        {/* <input type="hidden" name="redirect-url" value="URL_REDIRECCION" /> */}
                        <button className={classes.payButton} type="submit" >PAGAR</button>
                    </form>
                </Grid>

            </form>
        </>

    )

}

export default InfoForm;